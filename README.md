# npm-publish

Test npm publish with GitLab NPM registry feature

# Setup instrcutions

1. Get auth token for `.npmrc`

        curl -d "@auth.txt" -X POST http://localhost:3001/oauth/token

1. Replace `TOKEN` with auth token in `.npmrc`
1. Create project in GDK with group name `foo` and project name `bar`
1. Replace `PROJECT_ID` in `.npmrc`
1. Replace `PROJECT_ID` in `package.json`
1. Publish the package

        npm --verbose publish
